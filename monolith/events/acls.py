from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    parameters = {
        "query": f"{city} {state}",
    }
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=parameters)
    stuff = json.loads(response.content)

    pictures = {"picture_url": stuff["photos"][0]["src"]["original"]}

    return pictures


def get_weather_data(city, state):
    country = "US"
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{country}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    location = json.loads(response.content)
    lat = location[0]["lat"]
    lon = location[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_params = {
        "units": "imperial",
    }
    response_weather = requests.get(weather_url, weather_params)
    weather = json.loads(response_weather.content)
    get_weather = {
        "temp": weather["main"]["temp"],
        "weather description": weather["weather"][0]["description"],
    }
    return get_weather
